const Images = {
    iconHomeSelected : require('./iconHomeSelected.png'),
    iconHome: require('./iconHome.png'),
    iconMessagesSelected : require('./iconMessagesSelected.png'),
    iconMessages : require('./iconMessages.png'),

}

module.exports = Images;