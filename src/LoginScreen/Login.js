import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  Text,
  StyleSheet,
  ScrollView,
} from 'react-native';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onLoginClick = () => {
    this.props.navigation.navigate('Dashboard');
  };

  onSignUpClick = () => {
    this.props.navigation.navigate('signUp');

  };

  render() {
    return (
      <ScrollView
        style={styles.scrollView}
        contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
        <View style={styles.parentView}>
          <TextInput
            style={styles.textInputView}
            placeholder="Username"></TextInput>

          <TextInput
            style={styles.textInputView}
            placeholder="Password"></TextInput>

          <TouchableOpacity onPress={() => this.onLoginClick()}>
            <Text style={styles.loginButton}>Login</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.onSignUpClick()}>
            <Text style={styles.textView}>New User ? Click here to Signup</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    borderWidth: 1,
  },
  textInputView: {
    borderBottomWidth: 1,
    borderColor: '#DCDCDC',
  },
  textView: {
    padding: 10,
    textAlign:'center'
  },
  loginButton: {
    padding: 10,
    textAlign:'center',
    paddingHorizontal:20,
    backgroundColor:'orange'
  },
  parentView: {
    alignSelf: 'center',
    borderWidth: 1,
    width: '50%',
    borderColor: '#DCDCDC',
    alignItems: 'center',
  },
});
