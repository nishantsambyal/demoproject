import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import Reactotron from 'reactotron-react-native';
import CustomDateTimePickerDialog from 'react-native-custom-date-time-picker-dialog';
// import CustomDateTimePickerDialog from 'react-native-custom-date-time-picker-dialog';

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  class CounterApp extends Component {
    constructor(props) {
      super(props);
    }

    render() {
      Reactotron.log('hello rendering world');
      console.log(`props in counter app: ${JSON.stringify(this.props)}`);
      return (
        <View style={styles.parent}>
          <View style={styles.counterContainer}>
            <TouchableOpacity onPress={() => this.props.decreaseCounter()}>
              <Text style={styles.tappableText}>Decrease</Text>
            </TouchableOpacity>
            <Text>{this.props.counter}</Text>
            <TouchableOpacity onPress={() => this.props.increaseCounter()}>
              <Text style={styles.tappableText}>Increase</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                CustomDateTimePickerDialog.sampleMethod('one', 2, x => {
                  console.log(x);
                });
                // CustomDateTimePickerDialog.showToast("Hi there");
              }}>
              <Text style={styles.tappableText}>Test</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  },
);
function mapStateToProps(state) {
  return {
    counter: state.counterReducer.counter,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    // increaseCounter: () => dispatch({type: 'INCREASE_COUNTER'}),
    increaseCounter: () => dispatch(inc()),
    decreaseCounter: () => dispatch({type: 'DECREASE_COUNTER'}),
  };
}

function inc() {
  return {
    type: 'INCREASE_COUNTER',
  };
}
const styles = StyleSheet.create({
  parent: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  counterContainer: {
    flexDirection: 'row',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  tappableText: {
    padding: 10,
  },
});
