import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Login from '../LoginScreen/Login';
import SignUp from '../SignUpScreen/Signup';
import TabsNavigation from './TabsNavigation';

const loginNav = createStackNavigator({
  Login: Login,
  signUp: SignUp,
});

const switchNavigator = createSwitchNavigator(
  {
    loginNav: loginNav,
    Dashboard: TabsNavigation,
  },
  {
    initialRouteName: 'loginNav',
  },
);

export default createAppContainer(switchNavigator);
