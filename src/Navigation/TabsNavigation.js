import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Images from '../Asserts/Images/index';
import React from 'react';
import {Image, StyleSheet, TouchableOpacity, StatusBar} from 'react-native';

import CounterApp from '../CounterScreen/CounterApp';
import Results from '../ResultsScreen/Results';

const homeTabStack = createStackNavigator({
  counterApp: CounterApp,
});

const resultTabStack = createStackNavigator({
  resultApp: Results,
});

export default createBottomTabNavigator(
  {
    Home: homeTabStack,
    Result: resultTabStack,
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        switch (routeName) {
          case 'Home':
            return (
              <Image
                source={focused ? Images.iconHomeSelected : Images.iconHome}
                style={styles.container}
              />
            );
          case 'Result':
            return (
              <Image
                source={
                  focused ? Images.iconMessagesSelected : Images.iconMessages
                }
                style={styles.container}
              />
            );
        }
      },
      tabBarOptions: {
        activeTintColor: 'red',
        inactiveTintColor: 'green',
        style: {height: 50},
      },
    }),
  },
);

const styles = StyleSheet.create({
  container: {
    width: 24,
    height: 24,
  },
  headerLeftBack: {
    padding: 16,
    marginTop: Platform.OS === 'android' ? StatusBar.currentHeight + 16 : 0,
  },
});
