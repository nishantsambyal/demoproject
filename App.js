if(__DEV__) {
  import('./ReactotronConfig').then(() => console.log('Reactotron Configured'))
}
import React from 'react';
import Navigation from './src/Navigation/Navigation';
import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import { AsyncStorage } from 'react-native'
import counterReducer from './src/CounterScreen/redux/counterScreen.reducer';
import Reactotron from './ReactotronConfig';
const appReducer = combineReducers({
  counterReducer,
})
// const initialState = {
//   counter: 0,
// };

// const loginReducer = (state = initialState, action) => {
//   switch (action.type) {
//     case 'INCREASE_COUNTER':
//       return {counter: state.counter + 1};

//     case 'DECREASE_COUNTER':
//       if (state.counter > 0) return {counter: state.counter - 1};

//       default:
//           return state;
//   }
// }
const store = createStore(appReducer, Reactotron.createEnhancer());
console.log('store detail ',store.getState())
const App = () => {
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
};

export default App;
