#import "CustomDateTimePickerDialog.h"


@implementation CustomDateTimePickerDialog

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(sampleMethod:(NSString *)stringArgument numberParameter:(nonnull NSNumber *)numberArgument callback:(RCTResponseSenderBlock)callback)
{
    NSLog(stringArgument);

    // TODO: Implement some actually useful functionality
    callback(@[[NSString stringWithFormat: @"numberArgument: %@ stringArgument: %@", numberArgument, stringArgument]]);

}
RCT_EXPORT_METHOD(showToast:(NSString *)stringArgument){
    NSLog(stringArgument);
//   [self.view makeToast:@"This is a piece of toast."];
}

@end
